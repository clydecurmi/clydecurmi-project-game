﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour {

    public AudioClip MusciClip;
    public AudioSource MusicSource;
    Rigidbody2D rb;

	public Animator animator;

	public bool isGrounded = false;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        MusicSource.clip = MusciClip;
    }

    // Update is called once per frame
    void Update()
    {
		
        Movement();
		Jump ();
        Lose();
        Win();
        TextFile();

    }

    void Movement()
    {

		if (Input.GetKey (KeyCode.RightArrow)) {

			transform.Translate (Vector2.right * 6f * Time.deltaTime);
			transform.eulerAngles = new Vector2 (0, 0);

		} 

		if (Input.GetKey (KeyCode.LeftArrow)) {

			transform.Translate (Vector2.right * 6f * Time.deltaTime);
			transform.eulerAngles = new Vector2 (0, 180);
		} 


		if(Input.GetKeyDown("right")){
			animator.Play ("PlayerAnimation");

		}

		if(Input.GetKeyUp("right")){
			animator.Play ("None");

		}

		if(Input.GetKeyDown("left")){
			animator.Play ("PlayerAnimation");

		}

		if(Input.GetKeyUp("left")){
			animator.Play ("None");

		}

    }

	void Jump(){

		if (Input.GetButtonDown ("Jump") && isGrounded == true) {
		
			gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2 (0f, 10f), ForceMode2D.Impulse);
            MusicSource.Play();
        }

	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Finish") {

            Health.scoreValue -= 1 ;
            transform.position = new Vector3(0,0,0);

        }

        if (collision.gameObject.tag == "Key")
        {

            Score.scoreVal += 1;
            MusicSource.Play();

        }

        if (collision.gameObject.tag == "Spikes")
        {

            SceneManager.LoadScene("Lose");
            MusicSource.Play();

        }

        if (collision.gameObject.tag == "Lighting")
        {

            SceneManager.LoadScene("Lose");


        }
    }

    void Lose() {

        if (Health.scoreValue <= 0)
        {

            SceneManager.LoadScene("Lose");

        }

    }

    void Win()
    {

        if (Score.scoreVal == 3)
        {

            SceneManager.LoadScene("Win");

        }

    }

    void TextFile() {

        string path = Application.dataPath + "/Score.txt";
        if (!File.Exists(path)) {

            File.WriteAllText(path, "\n\n");

        }

        string content = Score.scoreVal + " points" + "\n";
        File.AppendAllText(path, PlayerPrefs.GetInt("tryNum") + " try " + content);

    }

}
